Contents of This File

Introduction
Requirements
Installation
Configuration


Introduction
Currently the autocomplete in the Link Field widget always shows content
suggestions from all content bundles.This module provides the
functionality to restrict certain content-entity bundle's from being 
referenced in link fields throughout the site.

Requirements
This module requires the following core module:

Link


Installation

Install as you would normally install a contributed Drupal module. Visit:
Installing Modules

Configuration


Simply activate the module, then go to 
/admin/config/system/link-field-entity-filter.
Do the settings as you like then see the action on the link fields
on content entities.

Similar project : Link Field Autocomplete Filter
