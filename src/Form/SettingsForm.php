<?php

namespace Drupal\link_field_entity_filter\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configure link_field_entity_filter settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'link_field_entity_filter_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['link_field_entity_filter.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['enable_filter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Link Filters'),
      '#description' => $this->t('Check the box to enbale the Filters'),
      '#default_value' => $this->config('link_field_entity_filter.settings')->get('enable_filter'),
    ];

    $negate = $this->config('link_field_entity_filter.settings')->get('negate');
    $default_value = NULL;
    if (!is_null($negate)) {
      $default_value = $negate === TRUE ? 1 : 0;
    }
    $form['negate'] = [
      '#type' => 'radios',
      '#options' => [
        '0' => $this->t('Include the selected below content types'),
        '1' => $this->t('Exclude the selected below content types'),
      ],
      '#title' => $this->t('Which content types should be allowed for internal links?'),
      '#default_value' => $default_value,
    ];

    // The allowed bundles.
    $form['allowed_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#description' => $this->t('If none are checked, then all are allowed.'),
      '#options' => link_field_entity_filter_all_content_types(TRUE),
      '#default_value' => $this->config('link_field_entity_filter.settings')->get('allowed_content_types') ?? [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('link_field_entity_filter.settings')
      ->set('enable_filter', $form_state->getValue('enable_filter'))
      ->set('negate', $form_state->getValue('negate'))
      ->set('allowed_content_types', $form_state->getValue('allowed_content_types'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
